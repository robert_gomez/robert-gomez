import processing.serial.*; 
import mqtt.*;

MQTTClient client;
Serial port; 

// Variable para los datos del sensor
int sensor_value; 

int lf = 10;
String serial_msg = null;

class Adapter implements MQTTListener {
  void clientConnected() {
    println("client connected");
    // ---------------------CAMBIAR AQUI-------------------------
    // Seleccionar aquí el tópico al que suscribirnos
    // El tópico base es /utec y luego el subtópico es /goodbye
    // Ejemplo: "/utec/sensorpedro"    
    client.subscribe("utec/receptorpeuy");
  }

  void messageReceived(String topic, byte[] payload) {
    println("new message: " + topic + " - " + new String(payload));
    port.write(new String(payload) + "\n");
  }

  void connectionLost() {
    println("connection lost");
  }
}

Adapter adapter;

void setup() {
  // Definir el cliente MQTT
  adapter = new Adapter();
  client = new MQTTClient(this, adapter);
  client.connect("mqtt://public:public@public.cloud.shiftr.io", "processing");
  
  // Imprimir en consola los puertos seriales que tenemos disponibles
  println(Serial.list()); //Serail port list
  
  // Abrir el puerto serial [1] (o sea, el segundo de la lista)
  port = new Serial(this, Serial.list()[1], 115200); 
  port.clear();
}

void draw() {
  while (port.available() > 0) {
    serial_msg = port.readStringUntil(lf);
    if (serial_msg != null) {
      // Imprimir en consola el mensaje que recibimos por Serial
      println(serial_msg);
      
      // ---------------------CAMBIAR AQUI-------------------------
      // Cambiar el tópico debajo en el que enviaremos los mensajes. 
      // El tópico base es /utec y luego el subtópico es /hello
      // Ejemplo: "/utec/sensorjuan"
      client.publish("utec/receptorpeuy", serial_msg);
    }
  }
}
