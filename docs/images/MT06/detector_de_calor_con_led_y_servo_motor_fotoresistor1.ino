// C++ code
//
#include <Servo.h>

Servo servo_2;
  int llum1 = 0;
void setup()
{
 
  pinMode(A1, INPUT);
  pinMode(13, OUTPUT);
  servo_2.attach(2, 500, 2500);
  pinMode(A0, INPUT);
  pinMode(8, OUTPUT);
  Serial.begin(9600);
 
 
 
}
void loop()
{
  llum1 = analogRead(A0);
  Serial.print("Sensor de llum: ");
  Serial.println(llum1);
  if (llum1>500)
  {
     digitalWrite(8, LOW);
  }
  else
  {
  digitalWrite(8, HIGH);
  }

  if (analogRead(A1) < 180) {
    digitalWrite(13, LOW);
    servo_2.write(90);
    delay(1000); // Espera por 1000 milisecondos
    servo_2.write(45);
    delay(1000); // Espera por 1000 milisecondos
  } 
  else {
    digitalWrite(13, HIGH);
    servo_2.write(0);
  }
  Serial.print("Sensor de TMP: ");
  Serial.println(analogRead(A1));
}