---
hide:
    - toc
---

# MI 01 Taller de emprendimientos
## Modulo intensivo   Taller de emprendimientos Fundacion da Vinci

Desarrollo del Modulo

![](../images/MI01/Talleremprendimientos.JPG)

Aprendimos herramientas para emprender en nuestro proyecto

Lo que fuimos viendo en este Modulo

![](../images/MI01/2021-09-06.png)

![](../images/MI01/2021-09-08.png)

![](../images/MI01/2021-09-08 1b.png)

![](../images/MI01/2021-09-09.png)

![](../images/MI01/2021-09-09 1b.png)

![](../images/MI01/2021-09-09 2b.png)

![](../images/MI01/2021-09-10.png)

Estuvieron buenas las dinamicas de grupo que formamos, grupo 6 que hasta el momento seguimos trabajando juntos.



Consigna Diseñar el cambas Bussnies de mi posible proyecto y diseñar un grafico de Gantt

![](../images/MI01/canvas.png)  Canvas Model

![](../images/MI01/Gantt.png)   Diagrama de Gantt

![](../images/MI01/Seguimiento proyecto.png)  

Seguimiento de Proyecto

Breve reseña

Quisiera contarles una breve reseña sobre el modelo de negocios que tome como ejemplo para
la creación del Model Bussnies Canvas propuesto por el curso.
La idea que tome surgió de la necesidad en la empresa que trabajo, que es del sector Logístico;
fabricar implementos para un brazo robótico que justamente se instalara cerca de una cinta
transportadora, para la selección automática de productos de varios tamaños, el cual hay que
diseñar y construir garras y demás soportes para tomar el producto y depositarlos por categoría
en una caja de plástico especial para esto, frente a la diversidad de tamaños de los productos,
por cada producto seleccionado debería llevar un implemento diferente según las característica
del mismo, en consecuencia pensé ya que tenemos este este problema el cual debe de repetirse
muy comúnmente en empresas que tienen robos de Licking deberían de tener las mismas
dificultades de integrar nuevos implementos y no esperar un largo tiempo para recibirlas y
también adecúalas a los distintos brazos robóticos que andan en el mercado local.
El plus de esto sería diseñar y construir implementos acordes a las necesidades del cliente, así
este se beneficiaría no solo del implemento a medida como de bajo coste y rápida fabricación
como también el seguimiento y mantenimiento de los productos brindados;
es desde aquí que surgió la idea de una necesidad real.

Referencias [ref] (https://descargarmodelo.com/descargar-modelo-canvas/)
