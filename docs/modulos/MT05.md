---
hide:
    - toc
---

# MT 05 Impresión 3D Fabricación aditiva / Escaneo 3D
## Impresión 3D Fabricación aditiva / Escaneo 3D

![](../images/Fabricacion_aditiva.jpg) ![](../images/artec_leo3.jpg){width=50%}


Aprendimos herramientas para el manejo de tecnologías de fabricación aditiva en impresión 3D. Principales temas: procesamiento de archivos para la generación de código-G y software relacionados, diseño de archivos, tipos de materiales, uso de máquinas, mantenimiento, diferentes tecnologías existentes en el contexto de Industria 4.0.
Escaneado 3D para prototipado rápido Incorporarás las herramientas básicas para el manejo de tecnologías de escaneo 3D. Se realizará un repaso de las distintas tecnologías de escaneo 3D que existen en el mundo, sus usos y aplicaciones y la implementación de estas tecnologías en el ámbito
profesional y educativo.

Fusion 360 ?

Escaneo 3d

![](../images/Mt05/artec.JPG)

[](https://www.artec3d.com/es/learning-center/3d-scanning-technology)
referencia: [Artec3d](https://www.artec3d.com/es/learning-center/3d-scanning-technology)


______________________________________________________________________
MT 05 Desafio:

"Modela (opcional escanear) un elemento en 3d que sea de utilidad para tu proyecto final o que pueda ser utilizado como insumo para su desarrollo. Documenta los distintos pasos del proceso en tu página web integrando los errores y aprendizajes obtenidos.

NOTA: Este módulo implica la producción de una pieza en un equipo de FDM por lo que el modelo debe entregarse en código G y en STL siguiendo las referencias detalladas a continuación.

Software para SLICING - CURA

Equipo para el que se realiza el Slicing - Ultimaker 3

Medidas 60x60x60mm máximo (es una prueba de impresión)

Variables a definir por parte del estudiante:

- Altura de capa
- Densidad de relleno
- Soporte
- Material
- Ventilación
- Velocidad
- Posición del objeto

**Las variables que debe definir el estudiante están listadas como ejemplo, pueden haber más variables a definir. "

La idea es diseñar una pieza estilo Lego que encaje en el dron a construir para acoplarle  la protección construida en el módulo de diseño 3d.

Software para diseño 3d Fusión 360..

1er Paso Diseño de la pieza 3d Modulo de acople Lego.

![](../images/Mt05/1.png){width="400"}

Imagen del acople.

![](../images/Mt05/2.png){width="400"}

Imagen de los protetores de helices.

![](../images/Mt05/3.png){width="400"}

Software para la creación del código G CURA para la impresora Ultimaker..

Cargar el archivo al programa CURA.


![](../images/Mt05/4.png){width="400"}

Pieza vista en el programa CURA

Variables a definir por parte del estudiante:ultimaker 3 g-code

- Altura de capa  01 mm
- Densidad de relleno  20%
- Soporte  
- Material  PLA 3.0
- Ventilación si al 100%
- Velocidad 70mm/s
- Posición del objeto  se volteo a 90° para tener mayor cuerpo de base
Adhesión a la cama si brim

![](../images/Mt05/5.png){width="400"}

Variables cargadas al Software CURA

![](../images/Mt05/6.jpg){width="400"}

Pieza protección de heclices impresa

![](../images/Mt05/7.png){width="400"}

Pieza modulo de acople dron



3r paso presentacion al prototipo

![](../images/Mt05/8.png){width="400"}

![](../images/Mt05/9.png){width="400"}

![](../images/Mt05/10.png){width="400"}

![](../images/Mt05/11.png){width="400"}
Tiempo de impresion pieza Modulo acople dron

Tips: Para una buena impresion calibrar la impresora con el filamento correcto y cuidar que la cama este nivelada, imprimir en lo posible piezas chicas con soporte a la cama de la impresora, cuidar el filamento durante la impresión por si no se corta durante el proceso.
