---
hide:
    - toc
---
![](../images/Desarrollo/Dron concep.PNG)

# Idea y Concepto
![](../images/Propuesta final/Presentacion Ingenuity Box Delivery Final 1.png)
La idea del proyecto surge de la necesidad en la empresa que trabajo de ciertos elementos para Realizar el delivery con drones ya existentes el trabajo diario en la logistica.

Concepto es construir un compartimiento como caja Delivery que al llegar cerca al local pueda abrir dos puertas para dejar caer el paquete que transporta mediante un circuito electronico.

La idea parte del Dron enviado a Marte el Ingenuity DRON que en su parte de abajo tiene una especie de caja donde estan los circuito electronicos.

El concepto es acoplarle a un Phanton 4 una especie de caja con dos puertas deslisantes para que al llegar al lugar indicado aterrice y esta caja atraves de un boton identifique que aterrizo y deje caer a poca altura el envios transportado
