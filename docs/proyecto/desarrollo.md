---
hide:
    - toc
---

# Desarrollo  ***

![](../docs/images/Propuesta final/memoria.PNG)

##¿Qué hace?
La idea es construir un compartimiento tipo box para acoplar al dron para que este transporte pequeñas encomiendas, este tiene un sistema de abrir y cerrar puertas corredizas que se accionan después de unos segundos, cuando el dron aterriza en el lugar indicado, este box delivery abre y deja caer la encomienda y vuelve a cerrar sus puertas para retornar al lugar de origen.


##        ¿Cómo llegaste a la idea?

![](../images/Propuesta final/idea.PNG)

La idea parte de la necesidad de realizar envíos de encomiendas seguras a través de drones delivery, el dron enviado a marte se ve claramente que en el dibujo que la electrónica y cerebro del dron está en una especie de caja.

##(Motivación)
El desafío surge de estar trabajando en la carrera de ingeniería en logística y una de las metas es poder realizar envíos de encomiendas a través de drones. Pero que este trate de llevarla en un compartimiento seguro y seco totalmente cerrado.

        ¿Quién lo ha hecho de antemano?
Existen muchos modelos de compartimientos para llevar cosas con drones.
Existe una empresa pionera en el Uruguay www.drone.uy que está realizando delivery en el latu en la ciudad de Montevideo Uruguay.

![](../images/Propuesta final/droneuy.PNG)

Pero su sistemas dejan caer la caja mediante una cuerda que en días de algún viendo dificulta mucho el descargar seguro.

##        ¿Qué diseñaste?
Una caja tipo box donde tiene dos compartimientos, uno en su parte de arriba tienen la electrónica reservada para su lugar y en la parte de abajo la mecánica para llevar la encomienda y dejarla caer de forma mecánica cuando el don aterrice.

##        ¿Qué materiales y componentes se utilizaron?
Los materiales utilizados fueron, una plancha acrílico  de 80 por 60 cm, Materia pla 3.0 para la impresora 3d ultimaker 3 medio rollo con las pruebas, tubo de aluminio  de 10 mm de diámetro 1 metro, 30 tornillos de 3 mm con tuerca, regatones de goma. Componentes: 1 Arduino UNO, 1 driver con chip ULN2003A, 2  motores paso a paso 28BYJ-48, cables y forro cable.

##        ¿Qué partes y sistemas se fabricaron?
El box para llevar la encomienda se diseñó y se cortó en laser cada una de las caras y se realizó encajes para que este quedara encajado y resistente, se fabricó las cremalleras y los engranajes para acoplarlos en los motores paso a paso, se diseñó los agarres para las patas del dron y la caja, como también las uniones en ángulo recto que sujetan cada una de las patas en PLA.

##        ¿Qué procesos se utilizaron?
Procesos diseños 2D y 3D en los programas Fusion  y Rhino 7.
Se utilizó el simulador de tinkercad para auduino, se trabajó con la impresora láser para el corte con su programa correspondiente, para la parte de programación se utilizó el arduino icloud por su facilidad de trabajo en la nube.

##        (Aditivos, sustractivos etc.)
Procesos aditivos Impresora Ultimaker 3. Procesos sustractivos corte laser y  fresado de algunos ajustes en el acrílico.

##        ¿Qué preguntas se respondieron?
Se trabajó primero en un prototipo realizados con piezas lego.
Después de realizado varios ajustes se llega un un prototipo de cartón, para después realizar los cortes e impresión con el material correspondiente.

##        ¿Qué funcionó? ¿Qué no?

![](../images/Propuesta final/prototipo1.PNG)

El primer prototipo como que depositaba la encomienda a mucha altura y las patas del dron para contener la caja quedaban muy largas esto dificultaría el aterrizaje de dron y se optó por patas más cortas, en consecuencia no podría tener puertas con bisagras porque, estas al depositar la caja no dejaría que se volviera a cerrar las puerta, por esto se optó por puertas corredizas con una cremallera y un engranaje en el motor.

##      ¿Cuáles son las conclusiones?
Los desafíos y problemas fueron muchos desde la construcción del compartimiento box, acomodar la electrónica donde poner los motores, como moverlos, como acoplar el box al dron, por esto esperar al corte definitivo para el final y realizar un prototipo en un material  más barato como cartón.

##        ¿Cuáles son los pasos a seguir?
Primeramente seguir perfeccionando el prototipo y presentarlo a la carrera de ingeniería en logística, después presentar la idea en algún concurso de emprendimiento ya que esto del delivery por aire promete mucho y en casos de desastres son una opción muy segura y barata de llevar medicamentos a zonas poco accesibles. Como segundo paso construir un dron mayor para llevar cargas de mayor peso y tamaño.


##     En el desarrollo:

Primeramente Trabajamos sobre la idea que en un principio era construir un dron, Conversando con colegas del Fab Lab Barcelona surgio la idea de contruir un accesorio del cual no hay mucha informacion, una caja acoplada a un dron para transportar objetos hasta 300 gr de peso a distancias de hasta 1 kilometro, y que al llegar al local indicado, despues de aterrizar deje caer  el paquete transportado  abriendo unas puertas y despues que se vuelvan a cerrar las mismas.

Primer Semana: se busca informacion referente a la construccion y materiales posibles a utilizar.

segunda Semana: se consulta a expertos sobre el tema y normas de vuelo de las mismas con estos conceptos, se asiste a un seminario de drones el la ciudad de Rivera ya que vinieron a disertar expertos en el tema de vuelo de drones a cargos de personal de Dinacia.

Tercer semana: se trabaja en prototipos de la parte electronica del compartimiento del dron delivery y se conversa con los asitentes de laboratios LABa para posibles visitas en proximas semanas



Cuarta Semana:

Se adjunta Diagrama de GANTT del Miro Proximamente.

Quinta Semana se trabaja en el diseño del prototipo

Sexta Semana se comienza a construir el primer prototipo en carton.

Septima semana se arma la caja con suscomponente mecanicos y se inserta su electronica.

 Octava semana se comienzan  con la programacion y las primera Pruebas.

 Novena semana se realizan ajustes y se trabaja sobre el trabajo final.

Decima  Semana prorroga  entrega y se concluye el trabajo final con la creacion del material multimedia.

![](../images/Idea y concepto/idea.PNG)

![](../images/Idea y concepto/cronograma.PNG)

![](../images/Idea y concepto/mayo.PNG)

![](../images/Idea y concepto/junio.PNG)

![](../images/Idea y concepto/dibujos.PNG)


###Materiales

ARCHIVOS EDITABLES del PROYECTO


![](../images/Desarrollo/efdia captura caja.PNG)

[CAJA DELIVERY DRON](../images/Desarrollo/Rhino_Robert_5.3dm)

[CAJA DELIVERY DRON 2](../images/Desarrollo/Rhino_Robert_6(1).3dm)

![](../images/Desarrollo/pieza caja dron.PNG)

[Pieza pata caja dron](../images/Desarrollo/pata dron.stl)

[Pieza brasalete caja dron](../images/Desarrollo/Brasalete dron.stl)

[pieza Boton](../images/Desarrollo/Boton caja dron.stl)

![Cremallera caja dron](../images/Desarrollo/cremallera.PNG)

[Pieza cremallera dron](../images/Desarrollo/Cremallera.stl)

[Software arduino abre y cierra BOX](../images/Desarrollo/Motor_va_y_viene_version_3.ino)
