---
hide:
    - toc
---

# Propuesta final

En la propuesta final se llego a esta caja Delivery DRON

![](../images/Propuesta final/Presentacion_Ingenuity_Box_Delivery_Final_1.png)


# Video Presentacion Trabajo final


[LINK Video Trabajo final](https://www.youtube.com/watch?v=i-bxsAG0ru4)

![](../images/Propuesta final/video.jpg)

[link YouTube](https://www.youtube.com/watch?v=i-bxsAG0ru4.mp4)
