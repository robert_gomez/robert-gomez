## Bienvenidos

![](.\images\Fabricacion_digital_sponsors.PNG)


![](.\images/utec.jpg)


Bienvenidos a mi sitio oficial sobre la Especialización Fabricación Digital e Innovación Abierta.

La Universidad Tecnológica de Uruguay (UTEC), en el marco del programa Uruguay Global, en alianza con el Banco Interamericano de Desarrollo (BID), Plan Ceibal y la Agencia Nacional de Investigación e Innovación (ANII), Realizán el lanzamiento de nuevos programas académicos, con el soporte de instituciones referentes a nivel mundial.

La Universidad Tecnológica del Uruguay (UTEC) ofrece una Especialización en Fabricación Digital e Innovación, en formato híbrido, con un fuerte componente emprendedor, co-dirigida y curada por Fab Lab Barcelona en el Instituto de Arquitectura Avanzada de Cataluña (IAAC).

Fab Lab Barcelona es el primer laboratorio de fabricación digital de la Unión Europea creado en 2007 a partir del Center for Bits and Atoms (CBA) del Massachusetts Institute of Technology (MIT).

La necesidad constante de diseñar nuevos productos y servicios que mejoren la competitividad sin dañar el medioambiente y a costos razonables, ha promovido que las tecnologías de fabricación digital cobren mayor protagonismo en todas las industrias. Esta especialización apunta a desarrollar profesionales capacitados en generar propuestas innovadoras y adaptadas a las necesidades de diferentes áreas de la producción y el diseño, haciendo uso de metodologías de innovación abierta asistidas por tecnologías de fabricación digital.

Fuente:https://neturuguay.com/2021/08/02/especializacion-en-fabricacion-digital-e-innovacion/

![](.\images\index\efdia.png)
##Participan:
 ![](.\images\Sponsors.PNG)
